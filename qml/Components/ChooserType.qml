/*
 * This file is based on work taken from Ubuntu Weather app as on September 2019. Copyright (C) 2019 UBports
 * Copyright (C) 2019 Joan Cibersheep
 *
 *
 * This App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 *
 * This App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Lomiri.Components 1.3

Item {
    height: typeSetting.height

    property string selectedType: "TOTP"

    ListModel {
        id: typeModel
        Component.onCompleted: initialize()

        function initialize() {
            typeModel.append({"text": "TOTP"})
            typeModel.append({"text": "HTOP"})
        }
    }

    ExpandableListItem {
        id: typeSetting
        listViewHeight: typeModel.count*units.gu(6.1)
        model: typeModel
        title.text: selectedType

        delegate: StandardListItem {
            title.text: model.text
            icon.name: "ok"
            icon.visible: selectedType === model.text
            onClicked: {
                selectedType = model.text;
                typeSetting.toggleExpansion();
            }
        }
    }
}
