import QtQuick 2.9
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

Dialog {
    id: infoDialog
    title: i18n.tr("Import Summary")

    property int impSuccess: 0
    property int impSkipped: 0
    property int impError:   0

    Text {
        text: i18n.tr("Keys imported successfully: ") + impSuccess
        wrapMode: Text.WordWrap
        color: theme.palette.normal.overlayText
    }

    Text {
        text: i18n.tr("Keys skipped: ") + impSkipped
        wrapMode: Text.WordWrap
        color: theme.palette.normal.overlayText
    }

    Text {
        text: i18n.tr("Keys with errors: ") + impError
        wrapMode: Text.WordWrap
        color: theme.palette.normal.overlayText
    }

    Button {
        text: i18n.tr("Close")

        onClicked: hide()
    }
}
