/*
 * This file is part of the 2FA Manager App for Ubuntu Touch
 * Copyright (C) 2019 Joan CiberSheep
 *
 * This App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 *
 * This App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Lomiri.Components 1.3

Rectangle {
    anchors.horizontalCenter: parent.horizontalCenter
    color: theme.palette.normal.background

    ProgressBar {
        visible: parent.visible
        width: parent.width
        anchors.verticalCenter: parent.verticalCenter
        indeterminate: true
    }
}
