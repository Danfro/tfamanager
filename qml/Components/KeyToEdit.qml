/*
 * This file is part of the 2FA Manager App for Ubuntu Touch
 * Copyright (C) 2019 Joan CiberSheep
 *
 * This App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 *
 * This App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Lomiri.Components 1.3

Column {
    spacing: units.gu(2)

    property var uriJson
    property bool editMode
    property bool isTOTP: typeTF.keyType == "TOTP"
    property bool isInfoEmpty: isTOTP
        ? issuerTF.text == "" || userTF.text == "" || intTF.text == "" || secretTF.text == ""
        : issuerTF.text == "" || userTF.text == "" || countTF.text == "" || secretTF.text == ""
    property alias userDesc: userDescTF.text

    onUriJsonChanged: {
        issuerTF.text = uriJson.issuer;
        userTF.text =  uriJson.label
        secretTF.text = uriJson.secret.b32

        if (isTOTP) {
            intTF.text = uriJson.period
        } else {
            typeTF.keyType = "HOTP"
            cType.selectedType = "HOTP"
            countTF.text = uriJson.counter
        }

        lengTF.passwordLength = uriJson.digits
        cLeng.selectedLeng = uriJson.digits

        algTF.algorithm = uriJson.algorithm
        cAlg.selectedAlg = uriJson.algorithm
    }

    Row {
        width: parent.width
        spacing: units.gu(2)

        Text {
            anchors.verticalCenter: parent.verticalCenter
            id: descTxt
            text: "<b>" + i18n.tr("Description") + "</b>"
            color: theme.palette.normal.overlayText
        }

        TextField {
            width: parent.width - units.gu(2) - descTxt.width
            id: userDescTF
            placeholderText: i18n.tr("Add a description")
            inputMethodHints: Qt.ImhNoPredictiveText

            onAccepted: {
                focus = false;
                issuerTF.focus = true;
            }
        }
    }

    Label {
        visible: !editMode
        text: "<b>" + i18n.tr("Paste an <i>otpauth://</i> URI") + "</b>"
        height: units.gu(6)
        anchors.horizontalCenter: parent.horizontalCenter
        verticalAlignment: Text.AlignBottom
        textSize: Label.Medium
    }

    Row {
        visible: !editMode
        width: parent.width
        spacing: units.gu(2)

        TextField {
            id: pasteURI
            width: parent.width -renderURI.width -units.gu(2)
            placeholderText: i18n.tr("Paste URI")
            inputMethodHints: Qt.ImhNoPredictiveText
            onAccepted: renderURI.clicked()
        }

        Button {
            id: renderURI
            text: i18n.tr("Add")
            color: theme.palette.normal.positive
            enabled: pasteURI.text != ""
            onClicked: fillInfo(pasteURI.text)
        }
    }

    Label {
        id: errorMsg
        visible: false
        width: parent.width
        wrapMode: Text.WordWrap
    }

    Label {
        visible: !editMode
        text: "<b>" + i18n.tr("Or type the information") + "</b>"
        height: units.gu(6)
        anchors.horizontalCenter: parent.horizontalCenter
        verticalAlignment: Text.AlignBottom
        textSize: Label.Medium
    }

    Row {
        width: parent.width
        spacing: units.gu(2)

        Text {
            anchors.top:parent.top
            anchors.topMargin: units.gu(2)
            id: typeTxt
            text: "<b>" + i18n.tr("Type") + "</b>"
            color: theme.palette.normal.overlayText
        }

        ListItem {
            id: typeTF
            width: parent.width - units.gu(2) - typeTxt.width
            height: cType.height > 0 ? cType.height : units.gu(7)
            divider.visible: false

            property string keyType: "TOTP"

            ChooserType {
                id: cType
                width: parent.width

                onSelectedTypeChanged: typeTF.keyType = selectedType
            }
        }
    }

    Row {
        width: parent.width
        spacing: units.gu(2)

        Text {
            anchors.verticalCenter: parent.verticalCenter
            id: issuerTxt
            text: i18n.tr("<b>Issuer</b>")
            color: theme.palette.normal.overlayText
        }

        TextField {
            width: parent.width - units.gu(2) - issuerTxt.width
            id: issuerTF
            placeholderText: i18n.tr("Who is providing the service")
            inputMethodHints: Qt.ImhNoPredictiveText

            onAccepted: {
                focus = false;
                userTF.focus = true;
            }
        }
    }

    Row {
        width: parent.width
        spacing: units.gu(2)

        Text {
            anchors.verticalCenter: parent.verticalCenter
            id: usrTxt
            text: "<b>" + i18n.tr("User") + "</b>"
            color: theme.palette.normal.overlayText
        }

        TextField {
            width: parent.width - units.gu(2) - usrTxt.width
            id: userTF
            placeholderText: i18n.tr("Who is using the service")
            inputMethodHints: Qt.ImhNoPredictiveText

            onAccepted: {
                focus = false;
                intTF.focus = true;
            }
        }
    }

    Column {
        width: parent.width

        Row {
            width: parent.width
            spacing: units.gu(2)

            Text {
                anchors.top: parent.top
                anchors.topMargin: units.gu(2)
                id: algTxt
                text: "<b>" + i18n.tr("Algorithm") + "</b>"
                color: theme.palette.normal.overlayText
            }

            ListItem {
                id: algTF
                width: parent.width - units.gu(2) - algTxt.width
                height: cAlg.height > 0 ? cAlg.height : units.gu(7)
                divider.visible: false

                property string algorithm: "SHA1"

                ChooserAlgorithm {
                    id: cAlg
                    width: parent.width

                    onSelectedAlgChanged: algTF.algorithm = selectedAlg
                }
            }
        }

        Row {
            width: parent.width
            spacing: units.gu(2)

            Text {
                anchors.top: parent.top
                anchors.topMargin: units.gu(2)
                id: passLenTxt
                text: "<b>" + i18n.tr("Password Length") + "</b>"
                color: theme.palette.normal.overlayText
            }

            ListItem {
                id: lengTF
                width: parent.width - units.gu(2) - passLenTxt.width
                height: cLeng.height > 0 ? cLeng.height : units.gu(7)
                divider.visible: false

                property string passwordLength: "6"

                ChooserLength {
                    id: cLeng
                    width: parent.width

                    onSelectedLengChanged: lengTF.passwordLength = selectedLeng
                }
            }
        }
    }

    Row {
        width: parent.width
        spacing: units.gu(2)
        visible: isTOTP

        Text {
            anchors.verticalCenter: parent.verticalCenter
            id: keyPeriod
            visible: true //uriJson.counter == undefined
            text: "<b>" + i18n.tr("Interval") + "</b>"
            color: theme.palette.normal.overlayText
        }

        TextField {
            width: parent.width - units.gu(2) - keyPeriod.width
            id: intTF
            text: "30"
            placeholderText: i18n.tr("Password life in seconds")
            inputMethodHints: Qt.ImhDigitsOnly

            onAccepted: {
                focus = false;
                secretTF.focus = true;
            }
        }
    }

    Row {
        width: parent.width
        spacing: units.gu(2)
        visible: !isTOTP

        Text {
            anchors.verticalCenter: parent.verticalCenter
            id: countTxt
            //visible: !keyPeriod.visible
            text: "<b>" + i18n.tr("Counter") + "</b>"
            color: theme.palette.normal.overlayText
        }

        TextField {
            width: parent.width - units.gu(2) - countTxt.width
            id: countTF
            placeholderText: i18n.tr("Password initial generation")
            inputMethodHints: Qt.ImhDigitsOnly

            onAccepted: {
                focus = false;
                secretTF.focus = true;
            }
        }
    }

    Text {
        text: "<b>" + i18n.tr("Secret Key") + "</b>"
        color: theme.palette.normal.overlayText
    }

    TextArea {
        id: secretTF
        width: parent.width
        placeholderText: i18n.tr("Secret key base")
        autoSize: true
        wrapMode: Text.WrapAnywhere
        inputMethodHints: Qt.ImhNoPredictiveText
    }

    function checkFields() {
        var isDescEmpty = userDescTF.text == "";
        var isIssuerEmpty = issuerTF.text == "";
        var isUserEmpty = userTF.text == ""
        var isIntEmpty = intTF.text == "";
        var isCountTFEmpty = countTF.text == "";
        var isSecretTFEmpty = secretTF.text == "";

        console.log(isDescEmpty, isIssuerEmpty, isIntEmpty, isCountTFEmpty, isSecretTFEmpty, algTF.algorithm)

        if (isTOTP) {
            var returnKey = {
                issuer: issuerTF.text,
                label: userTF.text,
                algorithm: algTF.algorithm,
                digits: lengTF.passwordLength,
                period: intTF.text,
                secret: OTPAuth.Secret.fromB32(secretTF.text) // Specially for old lib OTPAuth.Secret.fromB32()
            }
        } else {
            var returnKey = {
                issuer: issuerTF.text,
                label: userTF.text,
                algorithm: algTF.algorithm,
                digits: lengTF.passwordLength,
                counter: countTF.text,
                secret: OTPAuth.Secret.fromB32(secretTF.text) // Specially for old lib OTPAuth.Secret.fromB32()
            }
        }

        return returnKey;
    }

    function fillInfo(uri){
        console.log("Checking pasted URI")
        uri = root.checkUri(uri)

        try {
            uriJson = OTPAuth.URI.parse(uri);
        } catch(e) {
            console.log("Error",e);
            errorMsg.text = i18n.tr("URI format is invalid. The expected format is 'otpauth://totp/[ID]?secret=[CODE]`")
            errorMsg.visible = true
            hideErrorMsg.start()
        }
    }

    Timer {
        //General ticker. It ticks every second
        id: hideErrorMsg
        running: false
        repeat: false
        interval: 6000

        onTriggered: errorMsg.visible = false;
    }
}
