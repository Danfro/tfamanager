/*
 * This file is part of the 2FA Manager App for Ubuntu Touch
 * Copyright (C) 2019 Joan CiberSheep
 *
 * This App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 *
 * This App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

import QtQuick.LocalStorage 2.0
import "../js/otpauth.min.js" as OTPAuth
import "../js/db.js" as KeysDB

Page {
    id: backupPage
    objectName: "backupPage"
    anchors.fill: parent

    property string path
    //TODO: Don't hardcode it
    property string cachePath: "/home/phablet/.cache/tfamanager.cibersheep/"

    property int impSuccess
    property int impSkipped
    property int impError

    signal isExporting()
    signal doneExporting()
    signal isImporting(bool isJson)
    signal doneImporting(bool isJson)

    header: HeaderSettingsSpecial {
        id: backupHeader
        title: i18n.tr("Manage Backups")
        flickable: mainFlickable
    }

    ScrollView {
        width: parent.width
        height: parent.height
        contentItem: mainFlickable
    }

    Flickable {
        id: mainFlickable
        width: parent.width
        height: parent.height
        contentHeight: mainColumn.height
        topMargin: units.gu(2)
        bottomMargin: units.gu(6)

        Column {
            id: mainColumn
            width: parent.width
            anchors.top: parent.top
            anchors.topMargin: units.gu(1)
            spacing: units.gu(4)

            Label {
                width: parent.width
                text: i18n.tr("Export and import keys from a file")
                font.bold: true
                horizontalAlignment: Text.AlignHCenter
                anchors.horizontalCenter: parent.horizontalCenter
                wrapMode: Text.WordWrap
            }

            Column {
                width: parent.width
                spacing: units.gu(2)

                Label {
                    width: parent.width - units.gu(4)
                    text: i18n.tr("Export all keys in one json file. Keep in mind that keys are stored as plain text.")
                    horizontalAlignment: Text.AlignJustify
                    anchors.horizontalCenter: parent.horizontalCenter
                    wrapMode: Text.WordWrap
                }

                Label {
                    width: parent.width - units.gu(4)
                    text: i18n.tr("It's advisable to encrypt that file. The <a href='https://open-store.io/app/enigma.hummlbach'>Enigma</a> app could help here.")
                    horizontalAlignment: Text.AlignJustify
                    anchors.horizontalCenter: parent.horizontalCenter
                    wrapMode: Text.WordWrap
                    onLinkActivated: Qt.openUrlExternally(link)
                    linkColor: theme.palette.normal.activity
                }

                Button {
                    id: exportButton
                    text: i18n.tr("Export json file")
                    width: Math.min(parent.width, units.gu(25))
                    anchors.horizontalCenter: parent.horizontalCenter

                    onClicked: requestExport();

                    Connections {
                        target: backupPage

                        onIsExporting: importButton.visible = false;
                        onDoneExporting:importButton.visible = true;
                    }
                }

                IndefinedProgressBar {
                    id: exporting
                    visible: !exportButton.visible
                    width: exportButton.width
                    height: exportButton.height
                }
            }


            Column {
                width: parent.width
                spacing: units.gu(2)

                Label {
                    width: parent.width - units.gu(4)
                    text: i18n.tr("Import keys from a json file. Select a plain text json file.")
                    horizontalAlignment: Text.AlignJustify
                    anchors.horizontalCenter: parent.horizontalCenter
                    wrapMode: Text.WordWrap
                    onLinkActivated: Qt.openUrlExternally(link)
                }

                Button {
                    id: importButton
                    text: i18n.tr("Import json file")
                    width: Math.min(parent.width, units.gu(25))
                    anchors.horizontalCenter: parent.horizontalCenter

                    onClicked: requestImport();

                    Connections {
                        target: backupPage

                        onIsImporting: if (isJson) importButton.visible = false;
                        onDoneImporting: {
                            console.log("Debug: Finished importing")
                            if (isJson) importButton.visible = true;
                        }
                    }
                }

                IndefinedProgressBar {
                    id: importing
                    visible: !importButton.visible
                    width: importButton.width
                    height: importButton.height
                }
            }

            Column {
                width: parent.width
                spacing: units.gu(2)

                Label {
                    width: parent.width - units.gu(4)
                    text: i18n.tr("Import keys from a text file containing a list of 'otpauth://' URIs. Select a plain text json file.")
                    horizontalAlignment: Text.AlignJustify
                    anchors.horizontalCenter: parent.horizontalCenter
                    wrapMode: Text.WordWrap
                    onLinkActivated: Qt.openUrlExternally(link)
                }

                Button {
                    id: importTxtButton
                    text: i18n.tr("Import text file")
                    width: Math.min(parent.width, units.gu(25))
                    anchors.horizontalCenter: parent.horizontalCenter

                    onClicked: requestImportTxt();

                    Connections {
                        target: backupPage

                        onIsImporting: if (!isJson) importTxtButton.visible = false;
                        onDoneImporting: if (!isJson) importTxtButton.visible = true;
                    }
                }

                IndefinedProgressBar {
                    id: importingTxt
                    visible: !importTxtButton.visible
                    width: importTxtButton.width
                    height: importTxtButton.height
                }
            }
        }
    }

    Component {
        id: importFinishedDialog

        DialogImportResum {}
    }

    //TODO: Use c++ instead?
    function saveFile(fileUrl, text, xhrAction) {
        console.log("Requested:",xhrAction);
        var request = new XMLHttpRequest();
        request.open(xhrAction, fileUrl, false);
        request.setRequestHeader("Content-Type", 'application/json');

        request.onreadystatechange = function() {
            if (xhrAction == "GET" && request.readyState == XMLHttpRequest.DONE) {
                console.log("Json is ready");

                try {
                    renderBackupJson(JSON.stringify(JSON.parse(request.responseText)));
                } catch(e) {
                    //TODO: We might want to try if it's a text file
                    if (e instanceof SyntaxError) {
                        console.log("Syntx Error: JSON parse error");

                        if (request.responseText.indexOf("otpauth://") !== -1) {
                            console.log("We received a text file with URI");
                            renderUriFromTxt(request.responseText.split("\n"));
                        }
                    } else {
                        console.log("Error:",e);
                        impError += 1
                    }
                }
            }
        }

        request.onerror = function () {
            error(request, request.status);
        };

        request.send(text);
    }

    function generateBackupJson(data) {
        var jsonToReturn = {"keys": []}

        if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                jsonToReturn.keys.push({
                    "keyJSON": data.item(i).keyJSON,
                    "added": data.item(i).added,
                    "description": data.item(i).description
                });
            }
        }

        return JSON.stringify(jsonToReturn);
    }

    function renderUriFromTxt(array) {
        var needToInitDB = false;

        for (var j=0; j<array.length; j++) {
            var termFound  = null;

            //Calls to the KeysDB are syncronious
            termFound = KeysDB.findKey(array[j]);

            if (termFound === true) {
                console.log("The URI is in the db");
                impSkipped += 1
            } else {
                console.log("We can add the key to the db");

                KeysDB.storeKey(
                    Date(),
                    array[j],
                    ""
                );

                var needToInitDB = true;
                impSuccess += 1
            }
        }

        //Only initiate the DB if we did some changes to it
        if (needToInitDB) root.initDB();
    }

    function renderBackupJson(stringFile) {
        var needToInitDB = false;

        try {
            var currentFile = JSON.parse(stringFile);

            if ("keys" in currentFile) {
                console.log("Debug: Importing 2fa Manager json");

                for (var j=0; j<currentFile.keys.length; j++) {
                    //TODO: Search for key? user and issuer?
                    var termFound  = null;

                    //Calls to the KeysDB are syncronious
                    termFound = KeysDB.findKey(currentFile.keys[j].keyJSON);

                    if (termFound === true) {
                        console.log("The key is in the db");
                        impSkipped += 1;
                    } else {
                        console.log("We can add the key to the db");

                        KeysDB.storeKey(
                            currentFile.keys[j].added,
                            currentFile.keys[j].keyJSON,
                            currentFile.keys[j].description
                        );

                        var needToInitDB = true;
                        impSuccess += 1;
                    }
                }
            } else if ("tokens" in currentFile) {
                //TODO: Wha about issuerAlt?
                console.log("Debug: Importing FreeOTP+ json");

                for (var j=0; j<currentFile.tokens.length; j++) {
                    if (currentFile.tokens[j].issuerExt !== currentFile.tokens[j].issuerInt) {
                        console.log("DEBUG: Attenton, issuer are different. Ext",currentFile.tokens[j].issuerExt, "Int",currentFile.tokens[j].issuerInt)
                    }

                    if (currentFile.tokens[j].issuerExt !== "") {
                        var currentIssuer = currentFile.tokens[j].issuerExt;
                    } else if (currentFile.tokens[j].issuerInt !== "") {
                        var issuer = currentFile.tokens[j].issuerInt;
                    }

                    if (currentFile.tokens[j].type == "TOTP") {
                        var otpToAdd = new OTPAuth.TOTP({
                            issuer: currentIssuer,
                            label: currentFile.tokens[j].label,
                            algorithm: currentFile.tokens[j].algo,
                            digits: currentFile.tokens[j].digits,
                            period: currentFile.tokens[j].period,
                            secret: OTPAuth.Utils.b32.fromBuf(currentFile.tokens[j].secret)
                        });
                    } else {
                        var otpToAdd = new OTPAuth.HOTP({
                            issuer: currentIssuer,
                            label: currentFile.tokens[j].label,
                            algorithm: currentFile.tokens[j].algo,
                            digits: currentFile.tokens[j].digits,
                            counter: currentFile.tokens[j].counter,
                            secret: OTPAuth.Utils.b32.fromBuf(currentFile.tokens[j].secret)
                        });
                    }

                    otpToAdd = OTPAuth.URI.stringify(otpToAdd);

                    var termFound = null;
                    termFound = KeysDB.findKey(otpToAdd);

                    if (termFound === true) {
                        console.log("The token is in the db");
                        impSkipped += 1;
                    } else {
                        console.log("We can add the token to the db");

                        KeysDB.storeKey(
                            "",
                            otpToAdd,
                            ""
                        );

                        var needToInitDB = true;
                        impSuccess += 1;
                    }
                }
            } else {
                console.log("Error getting the 'type' of json");
                impError += 1;
            }
        } catch(e) {
            if (e instanceof SyntaxError) {
                console.log("Syntx Error: JSON parse error");
            } else {
                console.log("Error:",e, currentFile);
            }

            impError += 1;
        }

        //Only initiate the DB if we did some changes to it
        if (needToInitDB) root.initDB();
    }

    function requestExport() {
        isExporting();
        var date = new Date();
        //getMoth is 0-11
        var fileName = "export-" + date.getFullYear().toString() + String(date.getMonth() + 1) + date.getDate().toString()
            + date.getHours().toString() + date.getMinutes().toString() + date.getSeconds().toString() + ".json";
        var allKeys = KeysDB.getKeys();
        var saveJson = generateBackupJson(allKeys.rows);
        var exportJson = saveFile(cachePath + fileName, saveJson, "PUT");
        var exportPage = mainStack.push(Qt.resolvedUrl("PageHubExport.qml"));
        exportPage.path = cachePath + fileName;

        //Delete temporary file in cache
        exportPage.deleteCacheFile.connect(function() {
            //Doesn't work yet
            saveFile(cachePath + fileName, "", "DELETE");
        });

        exportPage.close.connect(function() {
            mainStack.pop();
        });

        doneExporting();
    }

    function requestImport() {
        //Send signal when import is started (true -> isJson)
        isImporting(true);
        impSuccess = 0
        impSkipped = 0
        impError = 0
        var importPage =  mainStack.push(Qt.resolvedUrl("PageHubImport.qml"), {"allowMultipleFiles":false});

        importPage.close.connect(function() {
            mainStack.pop();
            doneImporting(true);
        });

        importPage.accept.connect(function(urlOfItems) {
            for (var i=0; i<urlOfItems.length; i++) {
                console.log("DEBUG: Url",i,"Try to parse",urlOfItems[i]);
                saveFile(urlOfItems[i], "", "GET");
            }

            doneImporting(true);
            showDialog();
        });
    }

    function requestImportTxt() {
        //Send signal when import is started (false -> !isJson)
        isImporting(false);
        impSuccess = 0
        impSkipped = 0
        impError = 0
        var importPage = mainStack.push(Qt.resolvedUrl("PageHubImport.qml"), {"allowMultipleFiles":false});

        importPage.close.connect(function() {
            mainStack.pop();
            doneImporting(false);
        });

        importPage.accept.connect(function(urlOfItems) {
            for (var i=0; i<urlOfItems.length; i++) {
                console.log("DEBUG: Import from text",i,"Try to parse",urlOfItems[i]);
                saveFile(urlOfItems[i], "", "GET");
            }

            doneImporting(false);
            showDialog();
        });
    }

    function showDialog() {
        PopupUtils.open(importFinishedDialog, backupPage, {
            "impSuccess": impSuccess,
            "impSkipped": impSkipped,
            "impError": impError
        });
    }
}
