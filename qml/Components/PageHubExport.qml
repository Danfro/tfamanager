/*
 * This file is part of the 2FA Manager App for Ubuntu Touch
 * Copyright (C) 2019 Joan CiberSheep
 *
 * This App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 *
 * This App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Lomiri.Components 1.3
import Lomiri.Content 1.3

Page {
    id: exportPage
    objectName: "exportHubPage"
    anchors.fill: parent

    property var activeTransfer
    property var path

    signal close()
    signal deleteCacheFile()

    header: HeaderBase {
        title: i18n.tr("Export json file to")
    }

    Rectangle {
        anchors.fill: parent
        anchors.topMargin: exportPage.header.height

        ContentTransferHint {
            anchors.fill: parent
            activeTransfer: exportPage.activeTransfer
        }

        ContentItem {
            id: exportItem
        }

        ContentPeerPicker {
            visible: exportPage.visible
            showTitle: false
            handler: ContentHandler.Destination
            contentType: ContentType.Documents

            onPeerSelected: {
                activeTransfer = peer.request();
                stateChangeConnection.target = activeTransfer
                console.log("APP",activeTransfer.destination)
                console.log("Store",activeTransfer.store)
                console.log("DownID",activeTransfer.downloadId)
                var items = [];
                exportItem.url = path;
                console.log("1",JSON.stringify(exportItem))
                console.log("Export path:",path);
                items.push(exportItem);
                console.log("2",JSON.stringify(items))
                activeTransfer.items = items;
                activeTransfer.state = ContentTransfer.Charged;
            }

            onCancelPressed: {
                close();
            }
        }
    }

    Connections {
        id: stateChangeConnection
        target: null

        onStateChanged: {
            var statesForDebug = ["Created","Initiated","InProgress","Charged","Collected","Aborted","Finalized","Downloading","Downloaded"]
            console.log("DEBUG: activeTrasfer State is", statesForDebug[activeTransfer.state])

            switch (activeTransfer.state) {
                case ContentTransfer.Aborted:
                  console.log("Aborted")
                  break;
                case ContentTransfer.Collected:
                  console.log("Collected: Now finalize and close")
                  activeTransfer.finalize();
                  break;
                case ContentTransfer.Finalized:
                  console.log("Finalized")
                  deleteCacheFile();
                  close();
                  break;
                case ContentTransfer.InProgress:
                  console.log("InProgress")
                  break;
                case ContentTransfer.Charged:
                  console.log("Charged")
                  break;
                default:
                  console.log("Other state?",activeTransfer.state)
            }
        }
    }
}
