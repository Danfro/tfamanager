/*
 * This file is part of the 2FA Manager App for Ubuntu Touch
 * Copyright (C) 2019 Joan CiberSheep
 *
 * This App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 *
 * This App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Lomiri.Components 1.3
import Lomiri.Content 1.3

Page {
    id: importPage
    objectName: "importHubPage"

    property var activeTransfer
    property bool allowMultipleFiles

    signal accept(var files)
    signal close()

    header: HeaderBase {
        title: i18n.tr("Import backup file from")

        leadingActionBar.actions: Action {
            iconName: "back"
            text: i18n.tr("Back")

            onTriggered: close();
        }
    }

    Rectangle {
        anchors.fill: parent
        anchors.topMargin: importPage.header.height

        ContentTransferHint {
            anchors.fill: parent
            activeTransfer: importPage.activeTransfer
        }

        ContentPeerPicker {
            visible: importPage.visible
            showTitle: false
            contentType: ContentType.Documents
            handler: ContentHandler.Source

            onPeerSelected: {
                peer.selectionType = importPage.allowMultipleFiles
                    ? ContentTransfer.Multiple
                    : ContentTransfer.Single;

                importPage.activeTransfer = peer.request();
                stateChangeConnection.target = importPage.activeTransfer;
            }

            onCancelPressed: {
                close();
            }
        }
    }

    Connections {
        id: stateChangeConnection
        target: null

        onStateChanged: {
            var statesForDebug = ["Created","Initiated","InProgress","Charged","Collected","Aborted","Finalized","Downloading","Downloaded"]
            console.log("DEBUG: activeTrasfer State is", statesForDebug[activeTransfer.state])

            switch (activeTransfer.state) {
                case ContentTransfer.Aborted:
                  console.log("Aborted")
                  break;
                case ContentTransfer.Collected:
                  console.log("Finalize and close")
                  activeTransfer.finalize();
                  close();
                  break;
                case ContentTransfer.Finalized:
                  console.log("Finalized")
                  break;
                case ContentTransfer.InProgress:
                  console.log("InProgress")
                  break;
                case ContentTransfer.Charged:
                  var selectedItems = []

                  for (var i in importPage.activeTransfer.items) {
                    //console.log("Move imported items",importPage.activeTransfer.items[i].move(backupPage.cachePath))
                    selectedItems.push(
                        String(importPage.activeTransfer.items[i].url) //Don't remove .replace("file://", "")
                    );
                    console.log("Charged. Items:",selectedItems[i])
                  }

                  accept(selectedItems);
                  break;
                default:
                  console.log("Other state?",activeTransfer.state)
            }
        }
    }
}
