/*
 * This file is part of the 2FA Manager App for Ubuntu Touch
 * Copyright (C) 2019 Joan CiberSheep
 *
 * This App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 *
 * This App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtQuick.LocalStorage 2.0
import "../js/db.js" as KeysDB

Dialog {
    id: renderUri

    property var uriJson
    property string authType: keyPeriod.visible
        ? "TOTP"
        : "HTOP"
    title: i18n.tr("Add Authenticator Key")
    text: i18n.tr("%1 authenticator key").arg(authType)

    TextField {
        id: userDdescription
        placeholderText: i18n.tr("Add a description")
        inputMethodHints: Qt.ImhNoPredictiveText
    }

    Text {
        text: "<b>" + i18n.tr("Issuer") + ":</b> " + uriJson.issuer
        color: theme.palette.normal.overlayText
    }

    Text {
        text: "<b>" + i18n.tr("User") + ":</b> " + uriJson.label
        color: theme.palette.normal.overlayText
    }

    Text {
        text: "<b>" + i18n.tr("Algorithm") + ":</b> " + uriJson.algorithm
        color: theme.palette.normal.overlayText
    }

    Row {
        spacing: units.gu(2)

        Text {
            text: "<b>" + i18n.tr("Password Length") + ":</b> " + uriJson.digits
            color: theme.palette.normal.overlayText
        }

        Text {
            id: keyPeriod
            visible: uriJson.counter == undefined
            text: "<b>" + i18n.tr("Interval") + ":</b> " + uriJson.period
            color: theme.palette.normal.overlayText
        }

        Text {
            visible: !keyPeriod.visible
            text: "<b>" + i18n.tr("Counter") + ":</b> " + uriJson.counter
            color: theme.palette.normal.overlayText
        }
    }

    Text {
        text: "<b>" + i18n.tr("Key") + ":</b> " + uriJson.secret.b32
        color: theme.palette.normal.overlayText
        wrapMode: Text.WrapAnywhere
    }

    Button {
        text: i18n.tr("Add")
        color: theme.palette.normal.positive
        onClicked: addKey()
    }

    Button {
        text: i18n.tr("Cancel")
        onClicked: closingPop()
    }

    function addKey() {
        KeysDB.storeKey( Date(), uriToRender, userDdescription.text);
        root.initDB();
        renderUri.hide();
    }

    function closingPop() {
        renderUri.hide();
    }

    Component.onCompleted: {
        uriJson = OTPAuth.URI.parse(root.uriToRender);
        show();
    }

    Component.onDestruction: {
        //To be used when PopupBase is closed
    }

}
